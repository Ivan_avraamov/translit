({
    block: 'page',
    title: 'Title of the page',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: '_index.css' }
    ],
    scripts: [{ elem: 'js', url: '_index.js' }],
    content: [
        {
            block: 'header',
            content: [
                ''
            ]
        },
        {
            block: 'content',
            content: [
                {
                    block: 'translit',
                    js: 'inited',
                    content: [
                        {
                            block: 'input',
                            mix: {block: 'translit', elem: 'in'},
                            placeholder: 'cyr',
                            mods : {theme: 'normal', size: 'l', 'has-clear': true}
                        },
                        {
                            block: 'input',
                            mix: {block: 'translit', elem: 'out'},
                            placeholder: 'lat',
                            mods : {theme: 'normal', size: 'l', 'has-clear': true, 'changed': 'no'}
                        }
                    ]
                }
            ]
        },
        {
            block: 'footer',
            content: [
                ''
            ]
        }
    ]
})
