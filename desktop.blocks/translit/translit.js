modules.define('i-bem__dom', function(provide, DOM) {

    DOM.decl('translit' , {
        onSetMod : {
            'js' : {
                'inited' : function(){
                    this.bindTo('in', 'keyup', function(e){
                        // console.log(this.findBlockOn('in', 'input').elem('control').val());
                        var curCyr = this.findBlockOn('in', 'input').elem('control').val(),
                            _that = this.findBlockOn('out', 'input');
                        if(_that.hasMod('changed', 'no')){
                            _that.elem('control').val(DOM.blocks['cyr2lat'].cyr2lat(curCyr));
                        }
                    });
                    this.bindTo('out', 'keyup', function(){
                        this.findBlockOn('out', 'input').setMod('changed', 'yes');
                    });
                    this.bindTo('out', 'focusout', function(){
                        if(this.findBlockOn('out', 'input').elem('control').val() == ''){
                            this.findBlockOn('out', 'input').setMod('changed', 'no');
                        }
                        console.log(this.findBlockOn('out', 'input').elem('control').val());
                    })
                }
            }
        }
    })

provide(DOM);

});
